
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . "libraries/REST_Controller.php";

class S_Api_Controller extends REST_Controller {
  function __construct()
  {
      parent::__construct();
      $this->load->helper(['jwt', 'authorization']); 
  }

  protected function verify_request()
  {
    $headers = $this->input->request_headers();
    try {
      $token = isset($headers['Authorization']) ? $headers['Authorization'] : '';
      $data = AUTHORIZATION::validateToken($token);
      if ($data === false) {
        $response = ['status' => 401, 'message' => 'Unauthorized Access!'];
        $this->response($response, 401);
        exit();
      } else {
        return $data;
      }
    } catch (Exception $e) {
        $response = ['status' => 401, 'message' => 'Unauthorized Access! '];
        $this->response($response, 401);
    }
  }
}
