<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BASE_Controller extends MX_Controller {
  function __construct()
  {
      parent::__construct();
      $this->load->library('form_validation');
      $this->form_validation->CI = $this;
  }
}
