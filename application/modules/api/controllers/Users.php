<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends S_Api_Controller {

	public function index_get()
	{
		$tokenData = 'Hello World!';
    $token = AUTHORIZATION::generateToken($tokenData);
    // Set HTTP status code
    // Prepare the response
    $response = ['token' => $token];
    // REST_Controller provide this method to send responses
    $this->response($response, 200);
  }
  
  public function me_get(){
    $me = $this->verify_request();

    $this->response($me, 200);
  }
}
