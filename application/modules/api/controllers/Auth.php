<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends S_Api_Controller {

	public function login_post()
	{
    $email = $this->post('email');
    $password = $this->post('password');

    $users = $this->db->from('users')->where(
      [
        'email' => $email
      ]
    )->get()->result();


    $this->response([
      'status' => 200,
      'data' => $users,
      'payload' => $this->post()
    ],200);
  }
}
